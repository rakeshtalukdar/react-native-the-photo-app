
import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import Header from './components/Header'
import Camera from './components/Camera';

const App = () => {
  return (
    <View style={styles.container}>
      <Header title='The Camera App' />
      <Camera />
    </View>
  );
};



const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('screen').height - 130,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
})


export default App;
