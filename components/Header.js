import React from 'react';
import { View, Text, StyleSheet, Platform, StatusBar } from 'react-native';
const Header = (props) => {
    return (
        <View style={styles.header}>
            <Text style={styles.headerTitle}>{props.title}</Text>
        </View>
    );
}

Header.defaultProps = {
    title: 'The Camera App',
};

const styles = StyleSheet.create({
    header: {
        height: 70,
        padding: Platform.OS === "android" ? StatusBar.currentHeight : 10,
        backgroundColor: '#45B39D',
    },
    headerTitle: {
        textAlign: 'center',
        fontSize: 20,
        textTransform: 'uppercase',
        fontWeight: 'bold',
        color: '#fff',
    },

});

export default Header;