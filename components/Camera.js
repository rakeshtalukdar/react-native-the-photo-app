import React, { Component } from 'react';
import { View, Text, StyleSheet, PermissionsAndroid, TouchableOpacity, Alert, ToastAndroid } from 'react-native';

import { RNCamera } from 'react-native-camera';
import CameraRoll from "@react-native-community/cameraroll";
import Icon from 'react-native-vector-icons/FontAwesome';


class Camera extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFlashOn: false,
            isBackCamera: true,
        };
    }


    hasStoragePermission = async () => {
        try {
            const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
            const hasPermission = await PermissionsAndroid.check(permission);
            if (hasPermission) {
                return true;
            }
            const status = await PermissionsAndroid.request(permission);
            return status === 'granted';
        } catch (error) {
            throw error;
        }
    };

    showToastMessage = () => {
        ToastAndroid.showWithGravity(
            "Photo has been saved to local storage.",
            ToastAndroid.SHORT,
            ToastAndroid.TOP
        );
    };

    takePicture = async () => {
        try {
            if (this.camera) {
                const options = { quality: 0.5, base64: true };
                const data = await this.camera.takePictureAsync(options);
                const hasPermission = await this.hasStoragePermission();
                if (hasPermission === true) {
                    CameraRoll.save(data.uri, 'photo');
                    this.showToastMessage();
                } else {
                    Alert.alert('Error', 'Permission Not Granted!!', [{ text: 'OK' }]);
                }
            }
        } catch (error) {
            Alert.alert('Error', 'Permission Denied for Storage!!', [{ text: 'OK' }]);
        }
    };

    toggleFlash = () => {
        this.setState((prevState) => ({
            isFlashOn: !prevState.isFlashOn,
        }));
    };

    flipCamera = () => {
        this.setState((prevState) => ({
            isBackCamera: !prevState.isBackCamera,
        }));
    }

    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.cameraDisplay}
                    type={this.state.isBackCamera ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front}
                    flashMode={this.state.isFlashOn ? RNCamera.Constants.FlashMode.on : RNCamera.Constants.FlashMode.off}
                    captureAudio={false}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'Allow permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                />
                <View style={styles.cameraButtons}>
                    <TouchableOpacity onPress={this.toggleFlash} style={styles.buttons}>
                        {this.state.isFlashOn ?
                            <Icon style={{ color: '#fff' }} name="bolt" size={30} color="#900" />
                            :
                            <Icon style={{ color: '#767474' }} name="bolt" size={30} color="#900" />
                        }
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.takePicture} style={styles.capture}>
                        <Text style={{ fontSize: 14 }}>  </Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.flipCamera} style={styles.buttons}>
                        {this.state.isBackCamera ?
                            <Icon style={{ color: '#fff' }} name="camera" size={30} color="#900" />
                            :
                            <Icon style={{ color: '#767474' }} name="camera" size={30} color="#900" />
                        }
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    cameraDisplay: {
        flex: 1,
    },
    capture: {
        flex: 0,
        borderRadius: 100 / 2,
        backgroundColor: '#fff',
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    buttons: {
        flex: 0,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    cameraButtons: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
    },
});

export default Camera;